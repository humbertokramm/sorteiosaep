import pandas as pd
import collections
from random import randint
import time

# função só para destacar o nome
def destaque(n):
	linha = '------'
	for i in range(len(n)): linha += '-'
	print(linha)
	print('|  '+n+'  |')
	print(linha)

# Sorteio
def rodaSorteio(Rank,limite):
	# verifica quantos nomes participam deste sorteio
	n = len(Rank)
	# Cria uma lista
	sorteio=[]

	# Percorre todos os nomes da coleção
	for i in range(n):
		# Verifica se o nome possui tickets suficientes para o sorteio
		if(Rank[i][1]>=limite):
			# imprime o nome e o numero de tickets
			print(Rank[i][1],'tickets',Rank[i][0])
			# dá um tempinho para causar boa "impressão" ;)
			time.sleep(0.1)
			# Repete na lista o nome de acordo com a quantidade de tickets
			for k in range(Rank[i][1]):
				sorteio.append(i)
	# Faz o sorteio
	sorteado  = randint(0,len(sorteio)-1)
	# wait key
	a = input('').split(" ")[0]
	# Imprime o nome do sorteado em destaque
	destaque(Rank[sorteio[sorteado]][0])
	# wait key
	a = input('').split(" ")[0]
	# retorna o index do sorteado
	return sorteio[sorteado]

dir = 'D:\\Arquivos Pessoais\\Ondrive-H\OneDrive - Associacao Antonio Vieira\\Unisinos\\Aula\\DAEE\\sorteio\\'

# Nome dos arquivos de excel gerado pelo forms
names = [
	'empreendedorismo',
	'conecotres',
	'iot',
	'python',
	'IA',
	'rtos',
	'wifi'
]

# cria uma lista para guardar os nomes
namelList =[]

# busca as pessoas de cada atividade
for i in range(len(names)):
	# Carrega as planilhas para o banco de pessoas
	pessoas = pd.read_excel(dir+names[i]+'.xlsx', sheet_name='Planilha1')
	# Verifica a quantidade
	n = len(pessoas['Nome'])
	print(n,' e-mails da palestra de ', names[i])
	# se certifica que algum dado tenha sido carregado
	if(n>0):
		for j in range(n):
			# carrega os nomes deste banco para a lista de nomes
			namelList.append(pessoas['Nome'][j])

# Cria uma coleção
Rank = collections.Counter(namelList)
# Agrupa os nomes repetidos
Rank = Rank.most_common()
# Coloca em ordem alfabética
Rank = sorted(Rank)

print('\nPrimeiro Prêmio: Arduino Nano')
index = rodaSorteio(Rank,1)
# remove o nome do sortado pelo index
Rank.pop(index)

print('\nSegundo Prêmio: Esp32')
index = rodaSorteio(Rank,2)
# remove o nome do sortado pelo index
Rank.pop(index)

print('\nTerceiro Prêmio: Nucleo Board')
index = rodaSorteio(Rank,3)
